export const logger = {
    info: (msg) => { console.log(`${new Date().toLocaleString()} INFO: ${msg}`) },
    error: (msg) => { console.log(`${new Date().toLocaleString()} ERROR: ${msg}`)},
};
