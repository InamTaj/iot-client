import Axios from 'axios';
import * as queryString from 'query-string';
import * as caseConverter from 'change-object-case';

import { REQUEST_TYPES } from './endpoints';

export function apiCaller (
    {
        method = REQUEST_TYPES.GET,
        url = '',
        params = {},
        data = {},
    } = {},
) {

    // set-up case conversion configurations
    caseConverter.options = {recursive: true, arrayRecursive: true};

    return Axios({
        method,
        url,
        params,
        paramsSerializer: queryParams => queryString.stringify(queryParams),
        data,
        transformRequest: [reqData => JSON.stringify(caseConverter.snakeKeys(reqData))],
        transformResponse: [respData => caseConverter.toCamel(respData)],
        headers: {
            // 'Authorization': token || '',
            'Content-Type': 'application/json',
        },
        responseType: 'json',
        validateStatus: status => status >= 200 && status < 300,
    })
        .then(({ data: resp }) => resp)
        .catch(error => { throw(error); });
};
