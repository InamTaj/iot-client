import { logger } from './utils/logger';
import { apiCaller } from './utils/api-caller';
import { REQUEST_TYPES } from './utils/endpoints';
import {API_DELAY} from './utils/constants';
import {timeout} from './utils/timeout';


const dispatchCaller = async (URL) => {
    const hrstart = process.hrtime();
    const response = await apiCaller({
        method: REQUEST_TYPES.GET,
        url: URL,
    });
    const hrend = process.hrtime(hrstart);

    logger.info(`Response received successfully | RTT: ${hrend[0]}s ${hrend[1] / 1000000}ms`);
};

const loopScript = async (URL, delay) => {
    await dispatchCaller(URL);

    logger.info(`Delaying process for ${delay / 1000} seconds`);

    await timeout(delay);

    process.nextTick(async () => {
        await loopScript(URL, delay);
    });
};

const startRequestProcess = async (URL, shouldLoop, delay) => {
    logger.info(`Starting process for: ${URL}`);

    if (shouldLoop) {
        await loopScript(URL, delay);
    } else {
        await dispatchCaller(URL);
    }
};

const start = async () => {
    const[,, ...args] = process.argv;
    const urlIdx = args.indexOf('--url') > -1 ? args.indexOf('--url') : args.indexOf('-u');
    const shouldLoop = args.indexOf('--loop') > -1 || args.indexOf('-l') > -1;
    const delayIdx = args.indexOf('--delay');
    const delay = delayIdx > 0 ? args[ delayIdx + 1] * 1000 : API_DELAY;

    if (urlIdx === -1) {
        logger.error(`Please supply URL with --url OR -u switch.\nExample:\tyarn start --url localhost:3000`);
        process.exit(1);
    }

    try {
        await startRequestProcess(args[urlIdx + 1], shouldLoop, delay);
    } catch (e) {
        logger.error(`Something went wrong: ${e}`);
        process.exit(1);
    }
};

start();
