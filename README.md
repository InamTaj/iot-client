# iot-client
___
> Client software for IoT Devices



### First time Setup

- `cd iot-client`
- `yarn install` # installs dependencies


### Script Execution

- `yarn start --loop --url http://example.com`

Switches are as follows:
- `--url` OR `-u`: Defines URL which system will send a request to. This is a mandatory switch
- `--loop` OR `-l`: Defines whether the script should loop indefinitely or just execute once
- `--delay`: Defines the delay in seconds, by default if not provided, a 3 second delay will be used
